<?php

	/*************************
	*ログイン                *
	**************************/

	error_reporting(E_ALL & ~E_NOTICE);
	require_once './menu.php';
	require_once './login.php';

	echo logger();
	//ログイン状態のチェック
	if($_SESSION['login'] == 0){
		echo menu();
		echo login();
		echo copyright();
	}else {
		//ログイン処理の後、選択されていたプルダウンによってページ書き換え
		if($_POST['select'] == "add"){
			echo '<meta http-equiv="refresh" content="0; URL=./add.php">';
		}elseif($_POST['select'] == "manage" || $_POST['select'] == NULL){
			echo '<meta http-equiv="refresh" content="0; URL=./manage.php">';
		}if($_POST['select'] == "logout"){
			$_SESSION['login'] = 0;
			echo '<meta http-equiv="refresh" content="0; URL=./admin.php">';
		}
	}
?>
