<?php

	/*************************
	*削除関数                *
	**************************/
	
	function deleter(){
		$db = getDb();
		
		$str = " SELECT * from days where ";
		$i = 0;
		
		

		//deleteは一覧で選択したもの　delete2は確認画面で選択したもの
		if(isset($_POST['delete2']) == true){
			$delete2 = $_POST['delete2'];
		}elseif(isset($_POST['delete']) == true){
			$delete2 = $_POST['delete'];
		}
		foreach($delete2 as $delete){
			if($i != 0){
				$str = $str." or ";
			}
			$str = $str." no = ${delete} ";
			$i = 1;
		}
		//値に応じて処理のフラグ管理
		if(isset($_POST['delete2']) == true){
			$flag = 1;
		}
		if(isset($_POST['back']) == true){
			$flag = 0;
		}
		
		if($flag == 1){
			echo "・以下の予定を本当に削除しますか？<br>
				  ・一度削除した予定は復元できません。<br>　";
		}else{
			echo "・削除する予定をチェックして、実行ボタンを押してください。<br>　";
		}
		
		echo "<table border=\"1\">" ;
		echo "<form method = \"POST\">";
			
		echo "<tr style=\"background:#ccccff\"  width= \"40\">";
		if($flag != 1){
			echo "<td width= \"33\">削除</td>";
		}
		echo "<td>no</td>";
		echo "<td>開始日時</td>";
		echo "<td>終了日時</td>";
		echo "<td>題名</td>";
		echo "<td>公開状態</td>";
		echo "</tr>";

		$stt = $db -> query($str);			
		$stt->execute();
		
		
		while($row = $stt -> fetch(PDO::FETCH_ASSOC)){
				/*開始日時・終了日時のタイムスタンプ取得*/
				$time = mktime($row[hour],$row[minute],0,$row[month],$row[day],$row[year]);
				$etime = mktime($row[ehour],$row[eminute],0,$row[emonth],$row[eday],$row[eyear]);
				
				echo "<tr>";
				if($flag != 1){
					echo '<td><input type = "checkbox" name = "delete2[]" value = "'.$row[no].'" checked></td>';
				}
				echo "<td>".$row[no]."</td>";
				echo "<td>",date('Y年m月d日',$time),"(",$week[date('D',$time)],")";
				echo " ",date('H時i分',$time),"</td>";
				echo "<td>",date('Y年m月d日',$etime),"(",$week[date('D',$etime)],")";
				echo " ",date('H時i分',$etime),"</td><td>";
				echo $row[name]."</td>";
				echo "<td>";
				if($row[status] == 1){
					echo "公開";
				}else{
					echo "非公開";
				}
				echo "</td></tr>";
			}
			echo "</table>";
			//deleteの値保持
			foreach($_POST['delete'] as $delno){
				echo "<input type=\"hidden\" name=\"delete[]\" value=\"${delno}\">";
			}
			//削除実行のためにdelete2をdelete3へ格納（manage.phpで各変数で処理を分けているため）
			if(isset($_POST['delete2']) == true){
				foreach($_POST['delete2'] as $a){
					echo "<input type=\"hidden\" name=\"delete3[]\" value=\"${a}\">";
				}
			}
			if($flag == 1){
				echo"<input type = \"submit\" value =\"削除\" name = \"delsub\">";
				echo "<input type=\"submit\" name=\"back\" value=\"戻る\"></form>";
			}else{
				echo"<input type = \"submit\" value =\"削除\" name = \"del\">";
				echo "<input type=\"submit\" name=\"back2back\" value=\"戻る\"></form>";
			}

	}	
	
	function deletesql(){
		$db = getDb();
		
		$str = " DELETE from days where ";
	
		$strt = "予定 ";
		
		foreach($_POST['delete3'] as $delete){
			if($i != 0){
				$str = $str." or ";
			}
			$str = $str." no = ${delete} ";
			$i = 1;
			$strt = $strt."No.${delete} ";
		}
		echo $strt."を削除しました。";
		echo '<br><a href = "manage.php">管理画面に戻る</a>';
		
		$stt = $db -> query($str);			
		$stt->execute();

	}
?>