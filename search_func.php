<?php

	/*************************
	*検索画面関数            *
	**************************/
	

	function getsche(){
		$db = getDb();
		//半角スペースで文字列を分割
		$arr = explode(" ",$_POST['word']);
		if(isset($_POST['page']) == true){
			$page = $_POST['page'] * 10 - 10;
			$page = $page.",";
		}
		$str = " SELECT * FROM days where status = 1 ";
		$str2 = " order by month,day limit {$page}10 ";
		
		//空白の要素を削除
		$tmp = array();
		foreach($arr as $value){
			if($value != ""){$tmp[] = $value;}
		}
		$arr = $tmp;
		
		$i = 0;
		
		//var_dump($arr);
		
		
		/*
		for($i = 0 ; $i < count($words); $i++){
			$sqltext .= " concat_ws(' ',`name`,`detail`) LIKE ? ";
			if($i < (count($words)-1)){$sqltext .= ' '.$jyouken.' ';}
		}
		$sqltext .= ' ) ';
		*/
		
		for($i = 0 ; $i < count($arr); $i++){
			
			if($i == 0){
				$str = $str." and ";
			}else{
				$str = $str." {$_POST['andor']} ";
			}
			
			$str = $str." ( concat_ws(' ',`name`,`detail`) LIKE ? ) ";
		}

		$stt = $db -> prepare($str.$str2);
		
		//キーワード数分ループ
		for($i = 0 ; $i < count($arr); $i++){
			$num = (int)($i+1);
			$be = array('\\','_','%');
			$af = array('\\\\','\_','\%');
			$stt->bindvalue($num,'%'.str_replace($be,$af,$arr[$i]).'%');
		}

		echo "<hr>";
		
		//曜日用連想配列
		$week = array("Sun" => "日","Mon" => "月","Tue" => "火","Wed" => "水","Thu" => "木","Fri" => "金","Sat" => "土");
		

		//予定の内容取得に置き換え
		$getword = preg_replace('/ */','',$_POST['word']);
		
		

		//文字列が入力されていない場合等のエラー文
		if($getword == "" || $_POST['word'] == "" || isset($_POST['word']) == false){
			if(($getword == "" || $_POST['word'] == "") && isset($_POST['word']) == true){
				echo "正しい値を入力してください";
				
			}		
		}else{
			echo "<table border=\"1\" width= \"600\">" ;
			$stt->execute();
			echo "<b>検索結果一覧</b>";
			
			echo "<tr style=\"background:#ccccff\" >";
			echo "<td>開始日時</td>";
			echo "<td>終了日時</td>";
			echo "<td>題名</td>";
			echo "</tr>";
			
			while($row = $stt -> fetch(PDO::FETCH_ASSOC)){
				/*開始日時・終了日時のタイムスタンプ取得*/
				$time = mktime($row[hour],$row[minute],0,$row[month],$row[day],$row[year]);
				$etime = mktime($row[ehour],$row[eminute],0,$row[emonth],$row[eday],$row[eyear]);
		
				echo "<tr>";
				echo "<td>",date('Y年m月d日',$time),"(",$week[date('D',$time)],")";
				echo " ",date('H時i分',$time),"</td>";
				echo "<td>",date('Y年m月d日',$etime),"(",$week[date('D',$etime)],")";
				echo " ",date('H時i分',$etime),"</td><td>";
				if(isset($row[detail]) == true){
					echo '<a href = "detail.php?no='.$row[no].'">';
				}
				echo $row[name];
				if(isset($row[detail]) == true){
					echo "</a>";
				}
				echo "</td></tr>";
			}
			echo "</table>";
			
			//件数取得のsql文に置き換え
			$nstr = str_replace("*","count(*)",$str);
			$stt = $db -> prepare($nstr);
			//キーワード数分ループ
			for($i = 0 ; $i < count($arr); $i++){
				$num = (int)($i+1);
				$be = array('\\','_','%');
				$af = array('\\\\','\_','\%');
				$stt->bindvalue($num,'%'.str_replace($be,$af,$arr[$i]).'%');
			}
			$stt->execute();
			$rows = $stt -> fetchcolumn();
			echo searchpagemove($rows);
		}

	}
	
	function searchpagemove($rows){
		/*ページ数と遷移のための関数
		　1P10件なので、件数÷10の値でページ計測
		*/
		if($rows/10 > 1 || $_POST['page'] > 1){
			echo "<br><form method = \"POST\"><SELECT name=\"page\">";
			for($page = 1;$page < $rows/9 + 1;$page++){
				if($page == $_POST['page']){
					$selected = " selected";
				}
				echo "<OPTION value=\"{$page} \" {$selected} >{$page}ページ</OPTION>";
				$selected = "";
			}
			if(isset($_POST['word']) == true){
				$word = $_POST['word'];
			}
			
			//検索条件保持
			echo "<input type=\"hidden\" name=\"word\" value=\"${word}\">";
			echo "<input type=\"hidden\" name=\"andor\" value=\"${_POST['andor']}\">";
			echo "</SELECT><input type = \"submit\" value =\"表示\"></FORM>";
			//return $rows/9 + 1;
		}
	}
?>