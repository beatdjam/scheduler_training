<?php

	/*************************
	*DB接続                  *
	**************************/
	
function getDB(){
	$dsn = 'mysql:dbname=scheduler; host = 127.0.0.1';
	$usr = 'root';
	$pass = '';

	try{
		//DB connect
		$db=new PDO($dsn,$usr,$pass,array(PDO::ATTR_PERSISTENT => TRUE));
		//encode change
		$db->exec('SET NAMES utf8');
	}catch(PDOException $e){
		die("接続エラー: {$e->getMessage()}");
	}
	return $db;
}

?>