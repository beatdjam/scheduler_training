<?php

	/*************************
	*記事追加　関数          *
	**************************/

	function add(){
		require_once './textarea_test.php';

		$today = getdate();
		echo "<hr><b><font size = \"4\">";
		echo "新規登録";
		echo "<hr>";
		echo "</b>";
		echo "・予定を入力し、登録ボタンを押してください。<br>・題名は50文字、詳細は1000文字まで入力可能です。<br>";
		echo "<table><form method = \"POST\">";
		echo "<tr><td>題名</td>";
		echo "<td><input type=\"text\" name = \"title\" size = \"50\" maxlength = \"50\" value=\"${_POST['title']}\"></td></tr>";
		echo "<tr><td>開始日時</td><td><SELECT name=\"year\" STYLE=\"width:55px\">";
		for($year = 2011;$year <= 2021;$year++){
			if($today['year'] == $year && isset($_POST['year']) == false){
				$selected = " selected";
			}elseif($year == $_POST['year']){
				$selected = " selected";
			}
			echo "<OPTION value=\"${year}\" {$selected} >{$year}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>年";
		echo "<SELECT name=\"month\" STYLE=\"width:55px\">";
		for($month = 1;$month <= 12;$month++){
			if($month == $_POST['month']){
				$selected = " selected";
			}
			$month = sprintf("%02d",$month);
			echo "<OPTION value=\"${month}\" {$selected} >{$month}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>月";
		echo "<SELECT name=\"day\" STYLE=\"width:55px\">";
		for($day = 1;$day <= 31;$day++){
			if($day == $_POST['day']){
				$selected = " selected";
			}
			$day = sprintf("%02d",$day);
			echo "<OPTION value=\"${day}\" {$selected} >{$day}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>日";
		echo "<SELECT name=\"hour\" STYLE=\"width:55px\">";
		for($hour = 0;$hour <= 23;$hour++){
			if($hour == $_POST['hour']){
				$selected = " selected";
			}
			$hour = sprintf("%02d",$hour);
			echo "<OPTION value=\"${hour}\" {$selected} >{$hour}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>時";
		echo "<SELECT name=\"minute\" STYLE=\"width:55px\">";
		for($minute = 0;$minute <= 59;$minute++){
			if($minute == $_POST['minute']){
				$selected = " selected";
			}
			$minute = sprintf("%02d",$minute);
			echo "<OPTION value=\"${minute}\" {$selected} >{$minute}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>分";
		echo "</td></tr>";
		echo "<tr><td>終了日時</td><td><SELECT name=\"eyear\" STYLE=\"width:55px\">";
		echo "<OPTION value=\"60\" selected >開始</OPTION>";
		for($year = 2011;$year <= 2021;$year++){
			if($year == $_POST['eyear']){
				$selected = " selected";
			}
			echo "<OPTION value=\"${year}\" {$selected} >{$year}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>年";
		echo "<SELECT name=\"emonth\" STYLE=\"width:55px\">";
		echo "<OPTION value=\"60\" selected >開始</OPTION>";
		for($month = 1;$month <= 12;$month++){
			if($month == $_POST['emonth']){
				$selected = " selected";
			}
			$month = sprintf("%02d",$month);
			echo "<OPTION value=\"${month}\" {$selected} >{$month}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>月";
		echo "<SELECT name=\"eday\" STYLE=\"width:55px\">";
		echo "<OPTION value=\"60\" selected >開始</OPTION>";
		for($day = 1;$day <= 31;$day++){
			if($day == $_POST['eday']){
				$selected = " selected";
			}
			$day = sprintf("%02d",$day);
			echo "<OPTION value=\"${day}\" {$selected} >{$day}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>日";
		echo "<SELECT name=\"ehour\" STYLE=\"width:55px\">";
		echo "<OPTION value=\"60\" selected >開始</OPTION>";
		for($hour = 0;$hour <= 23;$hour++){
			if($hour == $_POST['ehour'] && isset($_POST['ehour']) == true){
				$selected = " selected";
			}
			$hour = sprintf("%02d",$hour);
			echo "<OPTION value=\"${hour}\" {$selected} >{$hour}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>時";
		echo "<SELECT name=\"eminute\" STYLE=\"width:55px\">";
		echo "<OPTION value=\"60\" selected >開始</OPTION>";
		for($minute = 0;$minute <= 59;$minute++){
			if($minute == $_POST['eminute'] && isset($_POST['eminute']) == true){
				$selected = " selected";
			}
			$minute = sprintf("%02d",$minute);
			echo "<OPTION value=\"${minute}\" {$selected} >{$minute}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>分";
		echo "</td></tr>";
		echo "</table>";
		echo area();
		echo "<br><input type = \"checkbox\" name = \"open\" value = \"1\">予定を公開する<br>";
		echo "</SELECT><input type = \"submit\" value =登録>";
		echo "<input type = \"reset\" value =リセット>";
		echo "</form>";
		//関数化できる気がするマン
	}

	function add_sql($sday,$eday,$status){
		$db = getDb();

		//詳細が入力されているかどうかをチェックし追記
		if($_POST['detail'] != ""){
			$detail = "detail,";
			$detail2 = ":detail,";
		}


		$str = "INSERT INTO days(year,month,day,name,${detail}hour,minute,eyear,emonth,eday,ehour,eminute,status)
				values(:year,:month,:day,:name,${detail2}:hour,:minute,:eyear,:emonth,:eday,:ehour,:eminute,:status)";

		$stt = $db -> prepare($str);

		$stt->bindValue(':year',$sday[0]);
		$stt->bindValue(':month',$sday[1]);
		$stt->bindValue(':day',$sday[2]);
		$stt->bindValue(':name',$_POST['title']);
		if($_POST['detail'] != ""){
			$stt->bindValue(':detail',$_POST['detail']);
		}
		$stt->bindValue(':hour',$sday[3]);
		$stt->bindValue(':minute',$sday[4]);
		$stt->bindValue(':eyear',$eday[0]);
		$stt->bindValue(':emonth',$eday[1]);
		$stt->bindValue(':eday',$eday[2]);
		$stt->bindValue(':ehour',$eday[3]);
		$stt->bindValue(':eminute',$eday[4]);
		$stt->bindValue(':status',$status);




		$stt->execute();
	}
?>
