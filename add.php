<?php
	/*************************
	*記事追加                *
	**************************/

	require_once './menu.php';
	require_once './adminmenu.php';
	require_once './add_func.php';
	
	//ログイン状態チェック
	if($_SESSION['login'] == 0){
	echo '<meta http-equiv="refresh" content="0; URL=./admin.php">';
	}else{
		echo menu();
		echo adminmenu();
	}
	
	
	$date[0] = $_POST['year'];
	$date[1] = $_POST['month'];
	$date[2] = $_POST['day'];
	$date[3] = $_POST['hour'];
	$date[4] = $_POST['minute'];
	$edate[0] = $_POST['eyear'];
	$edate[1] = $_POST['emonth'];
	$edate[2] = $_POST['eday'];
	$edate[3] = $_POST['ehour'];
	$edate[4] = $_POST['eminute'];
	
	//開始日と同じの指定になっている数値を置き換え
	for($i = 0;$i <= 4;$i++){
		if($edate[$i] == "60"){
			$edate[$i] = $date[$i];
		}
	}
	//送信されたか、また正しいデータが送られているかのチェック
	if(isset($_POST['year']) == true && 
		(checkdate($date[1],$date[2],$date[0]) == false ||
		checkdate($edate[1],$date[2],$date[0]) == false ||
		$time > $etime ||
		$_POST['title'] == "" ||
		mb_strlen($_POST['detail'],"UTF-8") > 1000)
	){
		//比較用タイムスタンプ生成
		$time = mktime($date[3],$date[4],0,$date[1],$date[2],$date[0]);
		$etime = mktime($edate[3],$edate[4],0,$edate[1],$edate[2],$edate[0]);
		
		/*
			時間があればエラーを変数のフラグによるチェックに変更した方が良い
		*/
		
		if(checkdate($date[1],$date[2],$date[0]) == false){
			echo "開始日時が不正です。<br>";
		}
		if(checkdate($edate[1],$edate[2],$edate[0]) == false){
			echo "終了日時が不正です。<br>";
		}
		if($time > $etime){
			echo "終了日時が開始日時より前に設定されています。<br>";
		}
		if($_POST['title'] == ""){
			echo "題名が入力されていません。<br>";
		}
		if(mb_strlen($_POST['detail'],'UTF-8') > 1000){
			echo "詳細が1000文字を超えています。";
		}
		echo add();
	}elseif(isset($_POST['year']) == true){
		echo "${date[0]}年${date[1]}月${date[2]}日の予定[${_POST['title']}]を登録しました。";
		if(isset($_POST['open']) == true){
			$status = 1;
		}else{
			$status = 0;
		}
		echo add_sql($date,$edate,$status);
		echo '<br><a href = "add.php">続けて登録する</a>';
		echo '<br><a href = "manage.php">管理画面に戻る</a>';
	}else{
		echo add();
	}
	echo copyright();
?>