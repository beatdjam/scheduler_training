<?php

	/*************************
	*編集画面                *
	**************************/
	
	require_once './menu.php';
	require_once './adminmenu.php';
	require_once './update_func.php';
	
	//ログイン判定
	if($_SESSION['login'] == 0){
	echo '<meta http-equiv="refresh" content="0; URL=./admin.php">';
	}else{
		echo menu();
		echo adminmenu();
	}

	//配列に各値を格納（$date[]で格納した方がきれいだけど書くとき分かりづらかったので）
	$date[0] = $_POST['year'];
	$date[1] = $_POST['month'];
	$date[2] = $_POST['day'];
	$date[3] = $_POST['hour'];
	$date[4] = $_POST['minute'];
	$edate[0] = $_POST['eyear'];
	$edate[1] = $_POST['emonth'];
	$edate[2] = $_POST['eday'];
	$edate[3] = $_POST['ehour'];
	$edate[4] = $_POST['eminute'];
	
	//開始と同じというプルダウンが選択されていた場合の置き換え
	for($i = 0;$i <= 4;$i++){
		if($edate[$i] == "60"){
			$edate[$i] = $date[$i];
		}
	}
	
	//入力されたデータのエラーチェック
	if(isset($_POST['year']) == true && 
		(checkdate($date[1],$date[2],$date[0]) == false ||
		checkdate($edate[1],$date[2],$date[0]) == false ||
		$time > $etime ||
		$_POST['title'] == "" ||
		mb_strlen($_POST['detail'],"UTF-8") > 1000)
	){
		$time = mktime($date[3],$date[4],0,$date[1],$date[2],$date[0]);
		$etime = mktime($edate[3],$edate[4],0,$edate[1],$edate[2],$edate[0]);
		
		/*
			エラーフラグチェックに変えた方がスマート
		*/
		
		if(checkdate($date[1],$date[2],$date[0]) == false){
			echo "開始日時が不正です。<br>";
		}
		if(checkdate($edate[1],$edate[2],$edate[0]) == false){
			echo "終了日時が不正です。<br>";
		}
		if($time > $etime){
			echo "終了日時が開始日時より前に設定されています。<br>";
		}
		if($_POST['title'] == ""){
			echo "題名が入力されていません。<br>";
		}
		if(mb_strlen($_POST['detail'],'UTF-8') > 1000){
			echo "詳細が1000文字を超えています。";
		}
		echo add();
	}elseif(isset($_POST['year']) == true){
		if(isset($_POST['open']) == true){
			$status = 1;
		}else{
			$status = 0;
		}
		if(isset($_POST['updbot']) == true){
			echo "${date[0]}年${date[1]}月${date[2]}日の予定[${_POST['title']}]を編集しました。";
			echo update_sql($date,$edate,$status);
		}elseif(isset($_POST['copybot']) == true){
			echo "${date[0]}年${date[1]}月${date[2]}日の予定[${_POST['title']}]を複製しました。";
			echo copysql($_POST['copy']);
		}


		echo '<br><a href = "manage.php">管理画面に戻る</a>';
	}else{
		echo add();
	}
	echo copyright();
?>