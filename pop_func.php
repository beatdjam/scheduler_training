<?php

	/*************************
	*予定一覧関数            *
	**************************/

		function popsche($year,$month,$day){
			$db = getDb();
			$today = getdate();

			if(isset($_GET['page']) == true){
				$page = $_GET['page'] * 10 - 10;
				$page = $page.",";
			} else {
				$page = 0;
			}

			//件数取得用
			$datestr2 = "の予定一覧";
			$str = " SELECT count(*) from days where status = 1 ";
			$str2 = " order by month,day limit ".$page."10";


			if(isset($_GET['year']) == true){
				$datestr = "{$_GET['year']}年";
				$str = $str." and year = {$_GET['year']}";
				if($_GET['month'] != ""){
					//var_dump($_GET['month']);
					$str = $str." and month = {$_GET['month']}";
					$datestr = $datestr."{$_GET['month']}月";
				}
				if($_GET['day'] != ""){
					$str = $str." and day = {$_GET['day']} ";
					$datestr = $datestr."{$_GET['day']}日";
				}
			}else{
				$str = $str." and year = ".$today['year']." and month = ".$today['mon']." and day = ".$today['mday'];
				$datestr = "{$today['year']} 年"."{$today['mon']} 月"."{$today['mday']} 日";
			}
			if($_GET['month'] != "" && $_GET['day'] != "" && $_GET['year'] != ""){
				if(checkdate($_GET['month'],$_GET['day'],$_GET['year'])){
					echo "<b><font size = \"4\">";
					echo $datestr.$datestr2;
					echo "</b></font>";
				}else{

					echo "<b><font size = \"4\">正しい日付を入力してください</font></b>";
				}
			}else{
				echo "<b><font size = \"4\">";
				echo $datestr.$datestr2;
				echo "</b></font>";
			}

			//echo $str.$str2;


			//予定の行数取得
			$stt = $db -> query($str);
			$stt->execute();


			$rows = $stt -> fetchcolumn();

			//echo $rows;


			//曜日用連想配列
			$week = array("Sun" => "日","Mon" => "月","Tue" => "火","Wed" => "水","Thu" => "木","Fri" => "金","Sat" => "土");

			echo "<table border=\"1\" width= \"600\">" ;

			echo "<tr style=\"background:#ccccff\" >";
			echo "<td>開始日時</td>";
			echo "<td>終了日時</td>";
			echo "<td>題名</td>";
			echo "</tr>";

			//予定の内容取得に置き換え
			$nstr = str_replace("count(*)","*",$str.$str2);
			$stt = $db -> query($nstr);
			$stt->execute();

			while($row = $stt -> fetch(PDO::FETCH_ASSOC)){
				/*開始日時・終了日時のタイムスタンプ取得*/
				$time = mktime($row[hour],$row[minute],0,$row[month],$row[day],$row[year]);
				$etime = mktime($row[ehour],$row[eminute],0,$row[emonth],$row[eday],$row[eyear]);

				echo "<tr>";
				echo "<td>",date('Y年m月d日',$time),"(",$week[date('D',$time)],")";
				echo " ",date('H時i分',$time),"</td>";
				echo "<td>",date('Y年m月d日',$etime),"(",$week[date('D',$etime)],")";
				echo " ",date('H時i分',$etime),"</td><td>";
				if(isset($row[detail]) == true){
					echo '<a href = "detail.php?no='.$row[no].'">';
				}
				echo $row[name];
				if(isset($row[detail]) == true){
					echo "</a>";
				}
				echo "</td></tr>";

			}
			echo "</table>";
			echo pagemove($rows);
		}

		function pagemove($rows){
			/*ページ数と遷移のための関数
			　1P10件なので、件数÷10の値でページ計測{
			*/
			if($rows/10 > 1){
				$today = getdate();
				echo "<br><form method = \"GET\"><SELECT name=\"page\">";
				for($page = 1;$page < $rows/9 + 1;$page++){
					if($page == $_GET['page']){
						$selected = " selected";
					}
					echo "<OPTION value=\"{$page} \" {$selected} >{$page}ページ</OPTION>";
					$selected = "";
				}
				if(isset($_GET['year']) == true){
					$now[year] = $_GET['year'];
					if($_GET['month'] != ""){
						$now[month] = $_GET['month'];
					}
					echo "test".$_GET['month']."]\n";
					if($_GET['day'] != ""){
						$now[day] = $_GET['day'];
					}
				}else{
					$now[year] = $today['year'];
					$now[month] = $today['mon'];
					$now[day] = $today['mday'];
				}
				//現在設定されている日付を送信する ページ遷移用
				echo "<input type=\"hidden\" name=\"year\" value=\"{$now[year]}\">";
				echo "<input type=\"hidden\" name=\"month\" value=\"{$now[month]}\">";
				echo "<input type=\"hidden\" name=\"day\" value=\"{$now[day]}\">";
				echo "</SELECT><input type = \"submit\" value =\"表示\"></FORM>";
				//return $rows/9 + 1;
			}
		}
	/*
	   ページ遷移の表示
	   予定詳細リンク
	*/

?>