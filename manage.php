<?php

	/*************************
	*管理画面                *
	**************************/
	
	require_once './menu.php';
	require_once './adminmenu.php';
	require_once './manage_func.php';
	require_once './delete.php';
	
	//ログイン判定
	if($_SESSION['login'] == 0){
	echo '<meta http-equiv="refresh" content="0; URL=./admin.php">';
	}else{
		//編集ボタンが押された場合で何も選択されていなかった場合の書き換え
		if(isset($_POST['upbot']) == true){
			if(isset($_POST['update']) == true){
				//編集で選択されていた値をGETでupdate.phpに送る
				echo "<meta http-equiv=\"refresh\" content=\"0; URL=./update.php?update=${_POST['update']}\">";
			}else{
				echo '<meta http-equiv="refresh" content="0; URL=./manage.php">';
			}
		}
		echo menu();
		echo adminmenu();
		
		//削除ボタンが押されて何も選択されていなかった場合
		if(isset($_POST['delbot']) == true || isset($_POST['del']) == true || isset($_POST['back']) == true){
			if(isset($_POST['delete']) == true || $_POST['back'] == true){
				//削除画面二つ目で戻るを押した場合の処理
				echo deleter();
			}elseif(isset($_POST['back']) == true){
				//削除画面の最初の確認画面で戻るを押した場合の処理
				echo manage();
			}else{
				echo manage();
			}
		}elseif(isset($_POST['delsub']) == true){
			//最後の確認画面で削除を押した場合の処理
			echo deletesql();
		}else{
				echo manage();
		}
		
		echo copyright();
	}

?>