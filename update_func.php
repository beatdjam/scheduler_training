<?php

	/*************************
	*編集画面関数            *
	**************************/
	

	function add(){
		require_once './textarea_test.php';
		
		$today = getdate();
		
		$db = getDb();
		$stt = $db -> prepare("SELECT * FROM days WHERE no = '${_GET[update]}'");
		$stt->execute();
		$row = $stt -> fetch(PDO::FETCH_ASSOC);
		
		//公開状態だった場合にチェックボックスをチェック済みにしておく
		if($row[status] == 1){
			$checked = "checked";
		}
		
		echo "<hr><b><font size = \"4\">";
		echo "NO.${_GET[update]}の予定を編集";
		echo "<hr>";
		echo "</b>";
		echo "・予定を入力し、登録ボタンを押してください。<br>　";
		echo "<table><form method = \"POST\">";
		echo "<tr><td>題名</td>";
		echo "<td><input type=\"text\" name = \"title\" size = \"50\" maxlength = \"50\" value=\"${row[name]}\"></td></tr>";
		echo "<tr><td>開始日時</td><td><SELECT name=\"year\" STYLE=\"width:55px\">";
		for($year = 2011;$year <= 2021;$year++){
			if($today['year'] == $year && isset($row['year']) == false){
				$selected = " selected";
			}elseif($year == $row['year']){
				$selected = " selected";
			}
			echo "<OPTION value=\"${year}\" {$selected} >{$year}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>年";
		echo "<SELECT name=\"month\" STYLE=\"width:55px\">";
		for($month = 1;$month <= 12;$month++){
			if($month == $row['month']){
				$selected = " selected";
			}
			$month = sprintf("%02d",$month);
			echo "<OPTION value=\"${month}\" {$selected} >{$month}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>月";
		echo "<SELECT name=\"day\" STYLE=\"width:55px\">";
		for($day = 1;$day <= 31;$day++){
			if($day == $row['day']){
				$selected = " selected";
			}
			$day = sprintf("%02d",$day);
			echo "<OPTION value=\"${day}\" {$selected} >{$day}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>日";
		echo "<SELECT name=\"hour\" STYLE=\"width:55px\">";
		for($hour = 0;$hour <= 23;$hour++){
			if($hour == $row['hour']){
				$selected = " selected";
			}
			$hour = sprintf("%02d",$hour);
			echo "<OPTION value=\"${hour}\" {$selected} >{$hour}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>時";
		echo "<SELECT name=\"minute\" STYLE=\"width:55px\">";
		for($minute = 0;$minute <= 59;$minute++){
			if($minute == $row['minute']){
				$selected = " selected";
			}
			$minute = sprintf("%02d",$minute);
			echo "<OPTION value=\"${minute}\" {$selected} >{$minute}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>分";
		echo "</td></tr>";
		echo "<tr><td>終了日時</td><td><SELECT name=\"eyear\" STYLE=\"width:55px\">";
		echo "<OPTION value=\"60\" selected >開始</OPTION>";
		for($year = 2011;$year <= 2021;$year++){
			if($year == $row['eyear']){
				$selected = " selected";
			}
			echo "<OPTION value=\"${year}\" {$selected} >{$year}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>年";
		echo "<SELECT name=\"emonth\" STYLE=\"width:55px\">";
		echo "<OPTION value=\"60\" selected >開始</OPTION>";
		for($month = 1;$month <= 12;$month++){
			if($month == $row['emonth']){
				$selected = " selected";
			}
			$month = sprintf("%02d",$month);
			echo "<OPTION value=\"${month}\" {$selected} >{$month}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>月";
		echo "<SELECT name=\"eday\" STYLE=\"width:55px\">";
		echo "<OPTION value=\"60\" selected >開始</OPTION>";
		for($day = 1;$day <= 31;$day++){
			if($day == $row['eday']){
				$selected = " selected";
			}
			$day = sprintf("%02d",$day);
			echo "<OPTION value=\"${day}\" {$selected} >{$day}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>日";
		echo "<SELECT name=\"ehour\" STYLE=\"width:55px\">";
		echo "<OPTION value=\"60\" selected >開始</OPTION>";
		for($hour = 0;$hour <= 23;$hour++){
			if($hour == $row['ehour'] && isset($row['ehour']) == true){
				$selected = " selected";
			}
			$hour = sprintf("%02d",$hour);
			echo "<OPTION value=\"${hour}\" {$selected} >{$hour}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>時";
		echo "<SELECT name=\"eminute\" STYLE=\"width:55px\">";
		echo "<OPTION value=\"60\" selected >開始</OPTION>";
		for($minute = 0;$minute <= 59;$minute++){
			if($minute == $row['eminute'] && isset($row['eminute']) == true){
				$selected = " selected";
			}
			$minute = sprintf("%02d",$minute);
			echo "<OPTION value=\"${minute}\" {$selected} >{$minute}</OPTION>";
			$selected = "";
		}
		echo "</SELECT>分";
		echo "</td></tr>";
		echo "</table>";
		echo area($row['detail']);
		echo "<br><input type = \"checkbox\" name = \"open\" value = \"1\" ${checked}>予定を公開する<br>";
		echo "</SELECT><input type = \"submit\" name = \"updbot\" value =登録>";
		echo "<hr>";
		echo "<input type = \"submit\" name=\"copybot\" value =複製>
				<input type = \"hidden\" name = \"copy\" value =\"${_GET[update]}\"></form>";
		//日付指定を関数化できる気がするマン
	}
	
	function update_sql($sday,$eday,$status){
		//編集、更新用SQL発行
		$db = getDb(); 
		
		//詳細が入力されていれば表示
		if($_POST['detail'] != ""){
			$detail = "detail = ";
			$detail2 = ":detail,";
		}
		
		$str = "UPDATE days SET year = :year,month = :month,day = :day,name = :name,${detail} ${detail2}
				 hour = :hour,minute = :minute,eyear = :eyear,emonth = :emonth,eday = :eday,ehour = :ehour,
				 eminute = :eminute,status = :status where no ='${_GET['update']}'";
		$stt = $db -> prepare($str);
			
		$stt->bindValue(':year',$sday[0]);
		$stt->bindValue(':month',$sday[1]);
		$stt->bindValue(':day',$sday[2]);
		$stt->bindValue(':name',$_POST['title']);
		if($_POST['detail'] != ""){
			$stt->bindValue(':detail',$_POST['detail']);
		}
		$stt->bindValue(':hour',$sday[3]);
		$stt->bindValue(':minute',$sday[4]);
		$stt->bindValue(':eyear',$eday[0]);
		$stt->bindValue(':emonth',$eday[1]);
		$stt->bindValue(':eday',$eday[2]);
		$stt->bindValue(':ehour',$eday[3]);
		$stt->bindValue(':eminute',$eday[4]);
		$stt->bindValue(':status',$status);
		
		/*
		echo "<br>";
		var_dump($stt);
		echo "<br>";
		var_dump($sday);
		echo "<br>";
		var_dump($eday);
		*/
					
		$stt->execute();
	}
	function copysql($no){
		//複製用SQL発行
		$db = getDb();
		$stt = $db -> prepare("SELECT * FROM days where no = '${no}'");
		$stt->execute();
		$row = $stt -> fetch(PDO::FETCH_ASSOC);
		
		//詳細の有無判定
		if($row['detail'] != null){
			$detail = "detail,";
			$detail2 = ":detail,";
		}
		
		$str = "INSERT INTO days(year,month,day,name,${detail}hour,minute,eyear,emonth,eday,ehour,eminute,status)
				values(:year,:month,:day,:name,${detail2}:hour,:minute,:eyear,:emonth,:eday,:ehour,:eminute,:status)";
			
		$stt = $db -> prepare($str);
			
		$stt->bindValue(':year',$row[year]);
		$stt->bindValue(':month',$row[month]);
		$stt->bindValue(':day',$row[day]);
		$stt->bindValue(':name',$row[name]);
		if($_POST['detail'] != ""){
			$stt->bindValue(':detail',$row[detail]);
		}
		$stt->bindValue(':hour',$row[hour]);
		$stt->bindValue(':minute',$row[hour]);
		$stt->bindValue(':eyear',$row[eyear]);
		$stt->bindValue(':emonth',$row[emonth]);
		$stt->bindValue(':eday',$row[eday]);
		$stt->bindValue(':ehour',$row[ehour]);
		$stt->bindValue(':eminute',$row[eminute]);
		$stt->bindValue(':status',$row[status]);
		
		$stt->execute();
	}
?>
