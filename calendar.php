<?php

	/*************************
	*カレンダー              *
	**************************/


	require_once './calendar_func.php';
	require_once './menu.php';
	date_default_timezone_set('Asia/Tokyo');
	//今日の日付取得
	$today = getdate();

	echo menu();

	//前月、次月の遷移　GETがなければ当月表示
	if(isset($_GET['month']) == true){
		if($_GET['month'] == +1){
			if($_SESSION['now']['mon'] == 12 && $_SESSION['now']['year'] == 2021){
				$_SESSION['now']['mon'] = 12;
				$_SESSION['now']['year'] = 2021;
			}elseif($_SESSION['now']['mon'] ==12){
				$_SESSION['now']['mon'] = 1;
				$_SESSION['now']['year'] +=1;
			}else{
				$_SESSION['now']['mon'] += 1;
			}
		}elseif($_GET['month'] == -1){
			if($_SESSION['now']['mon'] == 1 && $_SESSION['now']['year'] == 2011){
				$_SESSION['now']['mon'] = 1;
				$_SESSION['now']['year'] = 2011;
			}elseif($_SESSION['now']['mon'] ==1){
				$_SESSION['now']['mon'] = 12;
								$_SESSION['now']['year'] -=1;
			}else{
				$_SESSION['now']['mon'] -= 1;
			}
		}
	}else{
		$_SESSION['now'] = $today;
	}

	echo "<b><font size = \"4\">",$_SESSION['now']['year'],"年",$_SESSION['now']['mon'],"月","</font></b>","<hr>";
	if(($_SESSION['now']['mon'] == 1 && $_SESSION['now']['year'] == 2011) == false){
		echo "<a href=\"calendar.php?month=-1\">前の月</a>　";
	}else{
		echo "前の月　";
	}
	if(($_SESSION['now']['mon'] == 12 && $_SESSION['now']['year'] == 2021) == false){
		echo "<a href=\"calendar.php?month=+1\">次の月</a><br>";
	}else{
		echo "次の月　";
	}

	//月初、月末の取得
	$sday = getdate(mktime(0,0,0,$_SESSION['now']['mon'],1,$_SESSION['now']['year']));
	$eday = getdate(mktime(0,0,0,$_SESSION['now']['mon'] + 1,0,$_SESSION['now']['year']));


	//曜日表示
	echo "<table border=\"1\"   height = \"50\">" ;
	echo "<tr>";

	$weekday =array("日","月","火","水","木","金","土");
	foreach($weekday as $a){
		echo "<th style=\"background:#ccccff\" width= \"800\">";
		if($a == "日"){
			echo "<font color = \"red\">";
		}elseif($a == "土"){
			echo "<font color = \"blue\">";
		}
		echo "<center>",$a,"</center>";
		if($a == "日" || $a == "土"){
			echo "</font>";
		}
		echo "</th>";
	}
	echo "</tr><tr>";

	echo weekday($sday['weekday'],1);

	//カレンダー内描画　$nowdayは描画中の日付
	for($i = $sday['mday'];$i <= $eday['mday'];$i++){
		$dayname = holiday($holiday,$i);
		$nowday =getdate(mktime(0,0,0,$_SESSION['now']['mon'],$i,$_SESSION['now']['year']));
		if($today['mday'] == $i && $today['mon'] == $_SESSION['now']['mon']){
			echo "<td style = \"background:#ccffcc\">";
		}else{
			echo "<td>";
		}
		if($nowday['weekday']  == 'Sunday' || $holiday == 1){
			echo "<font color = \"red\">";
		}elseif($nowday['weekday']  == 'Saturday'){
			echo "<font color = \"blue\">";
		}
		echo $i,"<br>",$dayname,"　";
		echo "<br>",sche($i);

		if($nowday['weekday']  == 'Saturday'){
			echo "</font>";
			echo "</tr><tr>";
		}elseif($nowday['weekday']  == 'Sunday' || $holiday == 1){
			echo "</font>";
			$holiday = 0;
		}
		echo "</td>";
	}
	echo weekday($eday['weekday'],2);
	echo "</table>";

	if(($_SESSION['now']['mon'] == 1 && $_SESSION['now']['year'] == 2011) == false){
		echo "<a href=\"calendar.php?month=-1\">前の月</a>　";
	}else{
		echo "前の月　";
	}
	if(($_SESSION['now']['mon'] == 12 && $_SESSION['now']['year'] == 2021) == false){
		echo "<a href=\"calendar.php?month=+1\">次の月</a><br>";
	}else{
		echo "次の月　<hr>";
	}

	echo copyright();
?>

