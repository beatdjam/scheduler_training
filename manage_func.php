<?php

	/*************************
	*管理画面関数            *
	**************************/
	
		function manage(){
			$db = getDb();
			$today = getdate();
			
			//ページ数を取得してlimitを設定
			if(isset($_GET['page']) == true){
				$page = $_GET['page'] * 10 - 10;
				$page = $page.",";
			}
			
			//管理画面上で日付を指定して表示させようとした名残
			$datestr2 = "予定一覧";
			$str = " SELECT count(*) from days where status = 0 or status = 1";
			$str2 = " order by year,month,day limit {$page}10";

		
			if(isset($_GET['year']) == true){
				$datestr = "{$_GET['year']}年";
				$str = $str." and year = {$_GET['year']}";
				if($_GET['month'] != ""){
					//var_dump($_GET['month']);
					$str = $str." and month = {$_GET['month']}";
					$datestr = $datestr."{$_GET['month']}月";
				}
				if($_GET['day'] != ""){
					$str = $str." and day = {$_GET['day']} ";
					$datestr = $datestr."{$_GET['day']}日";
				}
			}
			if($_GET['month'] != "" && $_GET['day'] != "" && $_GET['year'] != ""){
				if(checkdate($_GET['month'],$_GET['day'],$_GET['year'])){
					echo "<b><font size = \"4\">";
					echo "(".$datestr.")".$datestr2;
					echo "</b></font>";	
				}else{
				
					echo "<b><font size = \"6\">エラー：正しい日付を入力してください</font></b>";
				}
			}else{
				echo "<b><font size = \"4\">";
				echo $datestr.$datestr2;
				echo "</b></font><hr>";	
				echo "・予定を選択し、編集ボタンか削除ボタンを押してください。<br>";
				echo "・予定の登録は<a href=\"add.php\">新規登録</a>から行う事ができます。<br>";
			}

			//echo $str.$str2;

			
			//予定の行数取得
			$stt = $db -> query($str);		
			$stt->execute();


			$rows = $stt -> fetchcolumn();
			
			//echo $rows;

			
			//曜日用連想配列
			$week = array("Sun" => "日","Mon" => "月","Tue" => "火","Wed" => "水","Thu" => "木","Fri" => "金","Sat" => "土");

			echo "<table border=\"1\">" ;
			
			echo "<tr style=\"background:#ccccff\"  width= \"40\">";
			echo "<td width= \"33\">編集</td>";
			echo "<td width= \"33\">削除</td>";
			echo "<td>no</td>";
			echo "<td>開始日時</td>";
			echo "<td>終了日時</td>";
			echo "<td>題名</td>";
			echo "<td>公開状態</td>";
			echo "</tr>";
			
			//予定の内容取得に置き換え
			$nstr = str_replace("count(*)","*",$str.$str2);
			$stt = $db -> query($nstr);			
			$stt->execute();
			
?>
			<form action = "manage.php" method = "POST">
			<input type = "submit"  name = "upbot" value = "編集">
			<input type = "submit" name = "delbot" value = "削除">
			<input type = "reset" value = "リセット">

<?php			
			while($row = $stt -> fetch(PDO::FETCH_ASSOC)){
				/*開始日時・終了日時のタイムスタンプ取得*/
				$time = mktime($row[hour],$row[minute],0,$row[month],$row[day],$row[year]);
				$etime = mktime($row[ehour],$row[eminute],0,$row[emonth],$row[eday],$row[eyear]);
				
				echo "<tr>";
				echo '<td><input type = "radio" name = "update" value = "'.$row[no].'"></td>';
				echo '<td><input type = "checkbox" name = "delete[]" value = "'.$row[no].'"></td>';
				echo "<td>".$row[no]."</td>";
				echo "<td>",date('Y年m月d日',$time),"(",$week[date('D',$time)],")";
				echo " ",date('H時i分',$time),"</td>";
				echo "<td>",date('Y年m月d日',$etime),"(",$week[date('D',$etime)],")";
				echo " ",date('H時i分',$etime),"</td><td>";
				if(isset($row[detail]) == true){
					echo '<a href = "detail.php?no='.$row[no].'">';
				}
				echo $row[name];
				//詳細が存在すればリンクに
				if(isset($row[detail]) == true){
					echo "</a></td>";
				}
				echo "<td>";
				//公開状態の判定
				if($row[status] == 1){
					echo "公開";
				}else{
					echo "非公開";
				}
				echo "</td></tr>";
			}
			echo "</table></form>";
			echo managepagemove($rows);
		}
		
		function managepagemove($rows){
			/*ページ数と遷移のための関数
			　1P10件なので、件数÷10の値でページ計測
			*/
			if($rows/10 > 1){
				$today = getdate();
				echo "<br><form method = \"GET\"><SELECT name=\"page\">";
				for($page = 1;$page < $rows/10 + 1;$page++){
					if($page == $_GET['page']){
						$selected = " selected";
					}
					echo "<OPTION value=\"{$page} \" {$selected} >{$page}ページ</OPTION>";
					$selected = "";
				}
				if(isset($_GET['year']) == true){
					$now[year] = $_GET['year'];
					echo "<input type=\"hidden\" name=\"year\" value=\"{$now[year]}\">";
					if($_GET['month'] != ""){
						$now[month] = $_GET['month'];
						echo "<input type=\"hidden\" name=\"month\" value=\"{$now[month]}\">";
					}
					echo "test".$_GET['month']."]\n";
					if($_GET['day'] != ""){
						$now[day] = $_GET['day'];
						echo "<input type=\"hidden\" name=\"day\" value=\"{$now[day]}\">";
					}
				}
				
				
				
				
			
				echo "</SELECT><input type = \"submit\" value =\"表示\"></FORM>";
				//return $rows/9 + 1;
				//onClick="location.href=''" value=
				
			}
		}	
?>